package forge

import (
	"os"
	"testing"
	"time"

	"code.gitea.io/sdk/gitea"
	"github.com/matryer/is"
)

func TestGitea_ReplaceIssueLinks(t *testing.T) {
	tt := []struct {
		Name     string
		Input    string
		Expected string
	}{
		{
			Name:     "single",
			Input:    "#1234",
			Expected: "[#1234](https://gitea.com/gitea/gitea/issues/1234)",
		},
		{
			Name:     "multiple",
			Input:    "#1234 #5678",
			Expected: "[#1234](https://gitea.com/gitea/gitea/issues/1234) [#5678](https://gitea.com/gitea/gitea/issues/5678)",
		},
	}

	var g Gitea
	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			assert := is.New(t)
			out := g.ReplaceIssueLinks(tc.Input)
			assert.Equal(out, tc.Expected) // Output should match expected
		})
	}
}

func TestGitea_Commit(t *testing.T) {
	if os.Getenv("INTEGRATION") == "" {
		t.Skip("skipping integration test; INTEGRATION not set")
	}
	assert := is.New(t)

	client, err := gitea.NewClient("https://gitea.com")
	assert.NoErr(err) // Should create client

	g := Gitea{Client: client}

	commit, err := g.Commit("827aa28a907853e5ddfa40c8f9bc52471a2685fd")
	assert.NoErr(err) // Should get commit

	expected := &Commit{
		// Ignoring Author, because it could feasibly change
		Message: "Merge pull request 'Dont Touch' (#12) from 6543-forks/test_repo:Add-Dont-Touch-Note into master\n\nReviewed-on: https://gitea.com/gitea/test_repo/pulls/12\nReviewed-by: techknowlogick <techknowlogick@gitea.io>\n",
		SHA:     "827aa28a907853e5ddfa40c8f9bc52471a2685fd",
		URL:     "https://gitea.com/gitea/test_repo/commit/827aa28a907853e5ddfa40c8f9bc52471a2685fd",
		Time:    time.Date(2020, 9, 1, 17, 55, 31, 0, time.UTC),
	}
	commit.Author = User{} // Ignoring Author, because it could feasibly change

	assert.Equal(commit, expected) // Commit should match expected
}

func TestGitea_Issue(t *testing.T) {
	if os.Getenv("INTEGRATION") == "" {
		t.Skip("skipping integration test; INTEGRATION not set")
	}
	assert := is.New(t)
	client, err := gitea.NewClient("https://gitea.com")
	assert.NoErr(err) // Should create client

	g := Gitea{Client: client}

	issue, err := g.Issue(2)
	assert.NoErr(err) // Should get issue

	expected := &Issue{
		// Ignoring User, because it could feasibly change
		Title:    "Spam",
		URL:      "https://gitea.com/gitea/test_repo/issues/2",
		Body:     ":(",
		Labels:   []string{"Invalid"},
		State:    "closed",
		Created:  time.Date(2020, 9, 1, 0, 23, 0, 0, time.UTC),
		Closed:   time.Date(2020, 9, 1, 14, 11, 37, 0, time.UTC),
		Comments: 2,
	}
	issue.User = User{} // Ignoring User, because it could feasibly change
	// Convert Gitea timestamps to UTC
	issue.Created = issue.Created.UTC()
	issue.Closed = issue.Closed.UTC()

	assert.Equal(issue, expected) // Issue should match expected
}

func TestGitea_File(t *testing.T) {
	if os.Getenv("INTEGRATION") == "" {
		t.Skip("skipping integration test; INTEGRATION not set")
	}
	assert := is.New(t)
	client, err := gitea.NewClient("https://gitea.com")
	assert.NoErr(err) // Should create client

	g := Gitea{Client: client}

	file, err := g.File("extend.md", "6543-patch-1")
	assert.NoErr(err) // Should get file

	expected := &File{
		Contents: "no plan to do",
		URL:      "https://gitea.com/gitea/test_repo/src/branch/6543-patch-1/extend.md",
	}

	assert.Equal(file, expected) // File should match expected
}
