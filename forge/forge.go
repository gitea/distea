package forge

import (
	"errors"
	"regexp"
	"time"
)

var (
	issueRe           = regexp.MustCompile(`#(\d+)`)
	ErrSourceFileOnly = errors.New("source is for files only")
)

// Commit is a git commit from a forge API
type Commit struct {
	Author    User
	Message   string
	SHA       string
	URL       string
	Time      time.Time
	Additions int
	Deletions int
	Verified  bool
}

// Issue is a forge issue
type Issue struct {
	User        User
	Title       string
	URL         string
	Body        string
	Labels      []string
	State       string
	Created     time.Time
	Closed      time.Time
	Comments    int
	PullRequest *PullRequest
}

// PullRequest is a forge pull request
type PullRequest struct {
	CanMerge bool
	Base     string
	Head     string
	Diff     string
	Patch    string
}

// User is a forge user
type User struct {
	Username string
	URL      string
	Image    string
}

// File is a forge file
type File struct {
	Contents string
	URL      string
}
