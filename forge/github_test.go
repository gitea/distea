package forge

import (
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/google/go-github/v43/github"
	"github.com/matryer/is"
)

func TestGitHub_ReplaceIssueLinks(t *testing.T) {
	tt := []struct {
		Name     string
		Input    string
		Expected string
	}{
		{
			Name:     "single",
			Input:    "#1234",
			Expected: "[#1234](https://github.com/go-gitea/gitea/issues/1234)",
		},
		{
			Name:     "multiple",
			Input:    "#1234 #5678",
			Expected: "[#1234](https://github.com/go-gitea/gitea/issues/1234) [#5678](https://github.com/go-gitea/gitea/issues/5678)",
		},
	}

	var gh GitHub
	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			assert := is.New(t)
			out := gh.ReplaceIssueLinks(tc.Input)
			assert.Equal(out, tc.Expected) // Output should match expected
		})
	}
}

func TestGitHub_Commit(t *testing.T) {
	if os.Getenv("INTEGRATION") == "" {
		t.Skip("skipping integration test; INTEGRATION not set")
	}
	assert := is.New(t)

	gh := GitHub{Client: github.NewClient(http.DefaultClient)}

	commit, err := gh.Commit("f32b0a9dfd09a60f616f29158f772cedd89942d2")
	assert.NoErr(err) // Should get commit

	expected := &Commit{
		// Ignoring Author, because it could feasibly change
		Message:   "Update README.md (#3)\n\nadd warning",
		SHA:       "f32b0a9dfd09a60f616f29158f772cedd89942d2",
		URL:       "https://github.com/go-gitea/test_repo/commit/f32b0a9dfd09a60f616f29158f772cedd89942d2",
		Time:      time.Date(2019, 11, 12, 21, 39, 27, 0, time.UTC),
		Additions: 2,
	}
	commit.Author = User{} // Ignoring Author, because it could feasibly change

	assert.Equal(commit, expected) // Commit should match expected
}

func TestGitHub_Issue(t *testing.T) {
	if os.Getenv("INTEGRATION") == "" {
		t.Skip("skipping integration test; INTEGRATION not set")
	}
	assert := is.New(t)

	gh := GitHub{Client: github.NewClient(http.DefaultClient)}

	issue, err := gh.Issue(1)
	assert.NoErr(err) // Should get issue

	expected := &Issue{
		// Ignoring User, because it could feasibly change
		Title:   "Please add an animated gif icon to the merge button",
		URL:     "https://github.com/go-gitea/test_repo/issues/1",
		Body:    "I just want the merge button to hurt my eyes a little. 😝 ",
		Labels:  []string{"bug", "good first issue"},
		State:   "closed",
		Created: time.Date(2019, 11, 9, 17, 0, 29, 0, time.UTC),
		Closed:  time.Date(2019, 11, 12, 20, 22, 22, 0, time.UTC),
	}
	issue.User = User{} // Ignoring User, because it could feasibly change

	assert.Equal(issue, expected) // Issue should match expected
}

func TestGitHub_File(t *testing.T) {
	if os.Getenv("INTEGRATION") == "" {
		t.Skip("skipping integration test; INTEGRATION not set")
	}
	assert := is.New(t)

	gh := GitHub{Client: github.NewClient(http.DefaultClient)}

	file, err := gh.File("README.md", "")
	assert.NoErr(err) // Should get file

	expected := &File{
		Contents: `# test_repo
Test repository for testing migration from github to gitea

Do not make any changes to this repo it is used for unit testing
`,
		URL: "https://github.com/go-gitea/test_repo/blob/master/README.md",
	}

	assert.Equal(file, expected) // File should match expected
}
