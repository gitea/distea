package forge

import (
	"context"
	"sort"

	"github.com/google/go-github/v43/github"
)

var (
	githubOrg  = "go-gitea"
	githubRepo = "gitea"
)

// GitHub is a forge implementation for https://github.com
type GitHub struct {
	Client *github.Client
}

// ReplaceIssueLinks replaces issue short-links with markdown
func (g *GitHub) ReplaceIssueLinks(in string) string {
	return issueRe.ReplaceAllString(in, "[#$1](https://github.com/go-gitea/gitea/issues/$1)")
}

// Image returns the Gitea image on GitHub
func (g *GitHub) Image() string {
	return "https://avatars.githubusercontent.com/u/12724356"
}

// Issue returns an Issue from GitHub
func (g *GitHub) Issue(num int) (*Issue, error) {
	issue, _, err := g.Client.Issues.Get(context.Background(), githubOrg, githubRepo, num)
	if err != nil {
		return nil, err
	}
	labels := make([]string, 0, len(issue.Labels))
	for _, label := range issue.Labels {
		labels = append(labels, label.GetName())
	}
	sort.Strings(labels)

	var pr *PullRequest
	if issue.IsPullRequest() {
		pull, _, err := g.Client.PullRequests.Get(context.Background(), githubOrg, githubRepo, num)
		if err != nil {
			return nil, err
		}
		pr = &PullRequest{
			CanMerge: pull.GetMergeable(),
			Base:     pull.GetBase().GetLabel(),
			Head:     pull.GetHead().GetLabel(),
			Diff:     pull.GetDiffURL(),
			Patch:    pull.GetPatchURL(),
		}
	}

	return &Issue{
		User: User{
			Username: issue.GetUser().GetLogin(),
			URL:      issue.GetUser().GetHTMLURL(),
			Image:    issue.GetUser().GetAvatarURL(),
		},
		Title:       issue.GetTitle(),
		URL:         issue.GetHTMLURL(),
		Body:        issue.GetBody(),
		Labels:      labels,
		State:       issue.GetState(),
		Created:     issue.GetCreatedAt(),
		Closed:      issue.GetClosedAt(),
		Comments:    issue.GetComments(),
		PullRequest: pr,
	}, nil
}

// Commit returns a Commit from GitHub
func (g *GitHub) Commit(sha string) (*Commit, error) {
	commit, _, err := g.Client.Repositories.GetCommit(context.Background(), githubOrg, githubRepo, sha, nil)
	if err != nil {
		return nil, err
	}
	return &Commit{
		Author: User{
			Username: commit.GetAuthor().GetLogin(),
			URL:      commit.GetAuthor().GetHTMLURL(),
			Image:    commit.GetAuthor().GetAvatarURL(),
		},
		Message:   commit.GetCommit().GetMessage(),
		SHA:       commit.GetSHA(),
		URL:       commit.GetHTMLURL(),
		Time:      commit.GetCommit().GetAuthor().GetDate(),
		Additions: commit.GetStats().GetAdditions(),
		Deletions: commit.GetStats().GetDeletions(),
		Verified:  commit.GetCommit().GetVerification().GetVerified(),
	}, nil
}

// File returns a File from GitHub
func (g *GitHub) File(path, branch string) (*File, error) {
	var opts *github.RepositoryContentGetOptions
	if branch != "" {
		opts = &github.RepositoryContentGetOptions{
			Ref: branch,
		}
	}
	file, dir, _, err := g.Client.Repositories.GetContents(context.Background(), githubOrg, githubRepo, path, opts)
	if err != nil {
		return nil, err
	}
	if dir != nil {
		return nil, ErrSourceFileOnly
	}
	content, err := file.GetContent()
	if err != nil {
		return nil, err
	}
	return &File{
		Contents: content,
		URL:      file.GetHTMLURL(),
	}, nil
}
