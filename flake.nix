{
  description = "Gitea Discord bot";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    distea = pkgs.buildGoModule rec {
      pname = "distea";
      version = "0.0.5";
      src = pkgs.nix-gitignore.gitignoreSource [] (builtins.path {
        name = pname;
        path = ./.;
      });
      subPackages = ["cmd/distea"];
      vendorHash = "sha256-hAv+Z0ZqfkC5OE9uj+F777xkuLdLv4PNrLdAFbZDDlY=";
      ldflags = ["-s" "-w" "-X=main.Version=${version}"];
      meta = with pkgs.lib; {
        description = "Gitea Discord bot";
        homepage = "https://gitea.com/gitea/distea";
        maintainers = with maintainers; [jolheiser];
        mainProgram = "distea";
      };
    };
  in {
    packages.${system}.default = distea;
    devShells.${system}.default = pkgs.mkShell {
      nativeBuildInputs = with pkgs; [
        go
        gopls
      ];
    };
    nixosModules.default = {
      pkgs,
      lib,
      config,
      ...
    }: let
      cfg = config.services.distea;
    in {
      options = with lib; {
        services.distea = {
          enable = mkEnableOption "Enable distea";

          package = mkOption {
            type = types.package;
            description = "distea package to use";
            default = distea;
          };

          config = mkOption {
            type = types.str;
            description = "config.toml path";
          };

          user = mkOption {
            type = types.str;
            default = "distea";
            description = "User account under which distea runs";
          };

          group = mkOption {
            type = types.str;
            default = "distea";
            description = "Group account under which distea runs";
          };
        };
      };
      config = lib.mkIf cfg.enable {
        users.users."${cfg.user}" = {
          group = "${cfg.group}";
          isSystemUser = true;
          isNormalUser = false;
          description = "user for distea service";
        };
        users.groups."${cfg.group}" = {};
        systemd.services.distea = {
          enable = true;
          script = let
            args = ["--config=${cfg.config}"];
          in "${cfg.package}/bin/distea ${builtins.concatStringsSep " " args}";
          wantedBy = ["multi-user.target"];
          after = ["network-online.target"];
          serviceConfig = {
            User = cfg.user;
            Group = cfg.group;
            Restart = "always";
            RestartSec = "15";
          };
        };
      };
    };
  };
}
