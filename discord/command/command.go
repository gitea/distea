package command

import (
	"code.gitea.io/distea/forge"

	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/gateway"
	"github.com/diamondburned/arikawa/v3/state"
	"github.com/diamondburned/arikawa/v3/utils/json/option"
	"github.com/rs/zerolog/log"
)

var (
	// Commands is a map of Command names to their Command
	Commands                     = make(map[string]Command)
	deferredInteraction          = api.InteractionResponse{Type: api.DeferredMessageInteractionWithSource}
	deferredInteractionEphemeral = api.InteractionResponse{Type: api.DeferredMessageInteractionWithSource, Data: &api.InteractionResponseData{Flags: api.EphemeralResponse}}
)

// Command is a discord command
type Command struct {
	Meta    api.CreateCommandData
	Handler func(*Context)
}

// Forge is a vcs that can return information to distea
type Forge interface {
	Image() string
	Commit(sha string) (*forge.Commit, error)
	Issue(num int) (*forge.Issue, error)
	File(path string, branch string) (*forge.File, error)
	ReplaceIssueLinks(content string) string
}

// Interface guard(s)
var (
	_ Forge = (*forge.GitHub)(nil)
	_ Forge = (*forge.Gitea)(nil)
)

// Context is passed in to each Command.Handler
type Context struct {
	App   *discord.Application
	State *state.State
	Event *gateway.InteractionCreateEvent
	Forge Forge
}

// DeferredError is a quick way to respond to a failed deferred interaction
func (c *Context) DeferredError(msg string) {
	data := api.EditInteractionResponseData{
		Content: option.NewNullableString(msg),
	}
	if _, err := c.State.EditInteractionResponse(c.App.ID, c.Event.Token, data); err != nil {
		log.Err(err).Msg("")
	}
}
