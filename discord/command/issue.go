package command

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/utils/json/option"
	"github.com/rs/zerolog/log"
)

func init() {
	Commands["issue"] = Issue
	Commands["View Issue(s)"] = IssueMessage
}

// Issue is the command for getting a forge.Issue by its number
var Issue = Command{
	Meta: api.CreateCommandData{
		Name:        "issue",
		Description: "Issue/PR information",
		Type:        discord.ChatInputCommand,
		Options: []discord.CommandOption{
			&discord.IntegerOption{
				OptionName:  "num",
				Description: "The issue/PR number",
				Min:         option.NewInt(1),
				Required:    true,
			},
		},
	},
	Handler: func(ctx *Context) {
		if err := ctx.State.RespondInteraction(ctx.Event.ID, ctx.Event.Token, deferredInteraction); err != nil {
			log.Err(err).Msg("")
			return
		}

		num, err := ctx.Event.Data.(*discord.CommandInteraction).Options[0].IntValue()
		if err != nil {
			log.Err(err).Msg("")
			ctx.DeferredError("could not parse num")
			return
		}

		embed, err := forgeIssue(ctx, int(num))
		if err != nil {
			log.Err(err).Msg("")
			ctx.DeferredError("could not retrieve issue/PR")
			return
		}

		data := api.EditInteractionResponseData{
			Embeds: &[]discord.Embed{
				embed,
			},
		}

		if _, err := ctx.State.EditInteractionResponse(ctx.App.ID, ctx.Event.Token, data); err != nil {
			log.Err(err).Msg("")
		}
	},
}

// IssueMessage is the message command for getting a forge.Issue by its number
var IssueMessage = Command{
	Meta: api.CreateCommandData{
		Name: "View Issue(s)",
		Type: discord.MessageCommand,
	},
	Handler: func(ctx *Context) {
		if err := ctx.State.RespondInteraction(ctx.Event.ID, ctx.Event.Token, deferredInteractionEphemeral); err != nil {
			log.Err(err).Msg("")
			return
		}

		var message discord.Message
		for _, msg := range ctx.Event.Data.(*discord.CommandInteraction).Resolved.Messages {
			message = msg
		}

		matches := findIssueMatches(message.Content)
		if len(matches) == 0 {
			ctx.DeferredError("no matches found for issues/PRs")
			return
		}

		embeds := make([]discord.Embed, 0, len(matches))
		for _, match := range matches {
			embed, err := forgeIssue(ctx, match)
			if err != nil {
				log.Err(err).Msg("")
				embed = discord.Embed{
					Title:       "Error",
					Description: fmt.Sprintf("Could not retrieve issue/PR #%d", match),
				}
			}
			embeds = append(embeds, embed)
		}

		data := api.EditInteractionResponseData{
			Embeds: &embeds,
		}

		if _, err := ctx.State.EditInteractionResponse(ctx.App.ID, ctx.Event.Token, data); err != nil {
			log.Err(err).Msg("")
		}
	},
}

func forgeIssue(ctx *Context, num int) (discord.Embed, error) {
	var embed discord.Embed

	issue, err := ctx.Forge.Issue(num)
	if err != nil {
		return embed, err
	}

	body := ctx.Forge.ReplaceIssueLinks(issue.Body)
	if len(body) > 1000 {
		body = body[:1000] + "..."
	}

	embed = discord.Embed{
		Author: &discord.EmbedAuthor{
			Name: issue.User.Username,
			URL:  issue.User.URL,
			Icon: issue.User.Image,
		},
		Thumbnail: &discord.EmbedThumbnail{
			URL: ctx.Forge.Image(),
		},
		Title:       fmt.Sprintf("%s #%d", issue.Title, num),
		URL:         issue.URL,
		Description: body,
		Footer: &discord.EmbedFooter{
			Text: "Created",
		},
		Timestamp: discord.NewTimestamp(issue.Created),
		Fields: []discord.EmbedField{
			{
				Name:   "Comments",
				Value:  strconv.Itoa(issue.Comments),
				Inline: true,
			},
		},
	}
	if len(issue.Labels) > 0 {
		embed.Fields = append(embed.Fields, discord.EmbedField{
			Name:   "Labels",
			Value:  fmt.Sprintf("`%s`", strings.Join(issue.Labels, "`, `")),
			Inline: true,
		})
	}
	state := issue.State
	if !issue.Closed.IsZero() {
		state += fmt.Sprintf(" <t:%d:R>", issue.Closed.Unix())
	}
	embed.Fields = append(embed.Fields, discord.EmbedField{
		Name:   "State",
		Value:  state,
		Inline: true,
	})
	if issue.PullRequest != nil {
		canMerge := "❌"
		if issue.PullRequest.CanMerge {
			canMerge = "✅"
		}
		embed.Fields = append(embed.Fields, []discord.EmbedField{
			{
				Name:   "Can Merge",
				Value:  canMerge,
				Inline: true,
			},
			{
				Name:   "URLs",
				Value:  fmt.Sprintf("[Diff](%s) • [Patch](%s)", issue.PullRequest.Diff, issue.PullRequest.Patch),
				Inline: true,
			},
			{
				Name:  "Branch Context",
				Value: fmt.Sprintf("`%s` ⬅️ `%s`", issue.PullRequest.Base, issue.PullRequest.Head),
			},
		}...)
	}

	return embed, nil
}

var issueRe = regexp.MustCompile(`(?:issues|pull)/(\d{1,5})|#(\d{1,5})`)

func findIssueMatches(content string) []int {
	var matches []int
	for _, match := range issueRe.FindAllStringSubmatch(content, -1) {
		m := match[1]
		if m == "" {
			m = match[2]
		}
		num, err := strconv.Atoi(m)
		if err != nil {
			log.Err(err).Msg("")
			continue
		}
		matches = append(matches, num)
	}
	return matches
}
