package command

import (
	"fmt"
	"strings"

	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/rs/zerolog/log"
)

func init() {
	Commands["social"] = Social
}

// Social is the social (media) command
var Social = Command{
	Meta: api.CreateCommandData{
		Name:        "social",
		Description: "Social info",
		Type:        discord.ChatInputCommand,
	},
	Handler: func(ctx *Context) {
		desc := make([]string, 0, len(socials))
		for _, social := range socials {
			desc = append(desc, fmt.Sprintf("[%s](%s)", social.name, social.link))
		}
		embed := discord.Embed{
			Thumbnail: &discord.EmbedThumbnail{
				URL: ctx.Forge.Image(),
			},
			Title:       "Gitea Social",
			Description: strings.Join(desc, "\n"),
		}
		data := api.InteractionResponse{
			Type: api.MessageInteractionWithSource,
			Data: &api.InteractionResponseData{
				Embeds: &[]discord.Embed{
					embed,
				},
			},
		}
		if err := ctx.State.RespondInteraction(ctx.Event.ID, ctx.Event.Token, data); err != nil {
			log.Err(err).Msg("")
		}
	},
}

type social struct {
	name string
	link string
}

var socials = []social{
	{"Discord", "https://discord.gg/Gitea"},
	{"Matrix", "https://matrix.to/#/%23gitea-space%3Amatrix.org"},
	{"Discourse", "https://forum.gitea.com/"},
	{"QQ Group (Chinese)", "https://qm.qq.com/cgi-bin/qm/qr?k=Ex-kCxSnTyxf-RM4oDihhPofPzLoDbXp&jump_from=webapi"},
	{"Twitter", "https://twitter.com/giteaio"},
	{"Mastodon", "https://social.gitea.io/@gitea"},
}
