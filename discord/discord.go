package discord

import (
	"context"
	"fmt"

	"code.gitea.io/distea/discord/command"

	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/gateway"
	"github.com/diamondburned/arikawa/v3/state"
	"github.com/rs/zerolog/log"
)

// Listen starts up the discord bot and returns a func to close down the gateway
func Listen(version, token, guildID string, forge command.Forge) func() error {
	snowflake, err := discord.ParseSnowflake(guildID)
	if err != nil {
		log.Err(err).Msg("")
		return nil
	}

	s := state.New("Bot " + token)
	s.AddIntents(gateway.IntentGuilds)

	app, err := s.CurrentApplication()
	if err != nil {
		log.Error().Msgf("failed to get application ID: %v", err)
		return nil
	}

	s.AddHandler(func(event *gateway.InteractionCreateEvent) {
		switch e := event.Data.(type) {
		case *discord.CommandInteraction:
			cmd, ok := command.Commands[e.Name]
			if !ok {
				log.Error().Msgf("unknown command %q", e.Name)
				return
			}
			cmd.Handler(&command.Context{
				App:   app,
				Event: event,
				State: s,
				Forge: forge,
			})
		default:
			log.Error().Msgf("unknown interaction: %#v", event)
		}
	})

	if err := s.Open(context.Background()); err != nil {
		log.Error().Msgf("failed to open: %v", err)
		return nil
	}
	log.Debug().Msgf("Discord is ready: https://discord.com/api/oauth2/authorize?client_id=%s&permissions=0&scope=bot%%20applications.commands", app.ID)
	if err := s.Gateway().Send(context.Background(), &gateway.UpdatePresenceCommand{
		Activities: []discord.Activity{
			{
				Name: fmt.Sprintf("v%s", version),
				Type: discord.GameActivity,
			},
		},
	}); err != nil {
		log.Err(err).Msg("could not update presence")
	}

	var commands []api.CreateCommandData
	for _, cmd := range command.Commands {
		commands = append(commands, cmd.Meta)
	}

	if _, err := s.BulkOverwriteGuildCommands(app.ID, discord.GuildID(snowflake), commands); err != nil {
		log.Err(err).Msg("")
	}

	return s.Close
}
