package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"

	"code.gitea.io/distea/discord"
	"code.gitea.io/distea/forge"

	"github.com/google/go-github/v43/github"
	"github.com/peterbourgon/ff/v3"
	"github.com/peterbourgon/ff/v3/fftoml"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
)

var Version = "develop"

func main() {
	logLevel := zerolog.InfoLevel

	fs := flag.NewFlagSet("distea", flag.ExitOnError)
	jsonFlag := fs.Bool("json", false, "Use JSON logging")
	fs.Func("log-level", "Set the logging level (default: info)", func(s string) error {
		lvl, err := zerolog.ParseLevel(s)
		if err != nil {
			return err
		}
		logLevel = lvl
		return nil
	})
	discordTokenFlag := fs.String("discord-token", "", "Discord bot token")
	guildIDFlag := fs.String("discord-guild-id", "", "Discord guild ID")
	forgeTokenFlag := fs.String("forge-token", "", "Forge token for authentication")

	fs.String("config", "", "Path to config")
	if err := ff.Parse(fs, os.Args[1:],
		ff.WithEnvVarPrefix("DISTEA"),
		ff.WithConfigFileFlag("config"),
		ff.WithAllowMissingConfigFile(true),
		ff.WithConfigFileParser(fftoml.New().Parse),
	); err != nil {
		log.Fatal().Err(err).Msg("")
	}

	zerolog.SetGlobalLevel(logLevel)
	if !*jsonFlag {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	httpClient := http.DefaultClient
	if *forgeTokenFlag != "" {
		ts := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: *forgeTokenFlag})
		httpClient = oauth2.NewClient(context.Background(), ts)
	}
	f := &forge.GitHub{
		Client: github.NewClient(httpClient),
	}
	closeGateway := discord.Listen(Version, *discordTokenFlag, *guildIDFlag, f)

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Kill, os.Interrupt)
	<-ch

	if err := closeGateway(); err != nil {
		log.Err(err).Msg("")
	}
}
