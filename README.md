# distea

[![Go Reference](https://pkg.go.dev/badge/code.gitea.io/distea.svg)](https://pkg.go.dev/code.gitea.io/distea)
[![Build Status](https://drone.gitea.com/api/badges/gitea/distea/status.svg?ref=refs/heads/main)](https://drone.gitea.com/gitea/distea)

A small Discord bot used in the [Gitea server](https://discord.gg/Gitea).

## Configuration

CLI flags:
```text
Usage of distea:
  -config string
        Path to config
  -discord-guild-id string
        Discord guild ID
  -discord-token string
        Discord bot token
  -forge-token string
        Forge token for authentication
  -json
        Use JSON logging
  -log-level value
        Set the logging level (default: info)
```

Config file (TOML):
```toml
discord-guild-id = ""
discord-token = ""
forge-token = ""
json = false
log-level = ""
```

Environment variables:
```text
DISTEA_DISCORD_GUILD_ID
DISTEA_DISCORD_TOKEN
DISTEA_FORGE_TOKEN
DISTEA_JSON
DISTEA_LOG_LEVEL
```


## License

[MIT](LICENSE)